require 'csv.rb' # Required for .csv parse
first_arg, *the_rest = ARGV #- extract first argument from command line

# Check for valid argument
if first_arg.to_i <= 0 then
  puts "Number of questions must be more than 0"
  exit
  
end

#parse the questions into an array
arr_of_arrs = CSV.read("questions.csv")

#initialise variables
$i = 0
$num = arr_of_arrs.length
$qnum = first_arg.to_i

# Create output array
questions_array= Array.new($qnum)

# Main itteration loop
  while $i < $qnum  do
      $new_question = rand($num) + 1 # Find a new question
      # Check for duplicate
      if questions_array.count($new_question) > 0
        # Duplicates OK if questions exceed available
        if $i > $num-1
          questions_array[$i] = $new_question
          $i +=1
        end
      else
        #Save question and continue
        questions_array[$i] = $new_question
        $i +=1
      end

  end
  # Print out array
  a = questions_array.reverse.drop(1).reverse
  a.each { |q| printf "%i , ", q }
  printf "%i ", questions_array.last